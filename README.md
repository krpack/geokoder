# geokodeR <a href='https://gitlab.com/krpack/geokoder'><img src='man/figures/logo.jpg' align="right" height="139" /></a>


<!-- badges: start -->
[![Project Status: Active. The project has reached a stable, usable state and is being actively developed.](https://www.repostatus.org/badges/latest/active.svg)](https://www.repostatus.org/#active)
[![packageversion](https://img.shields.io/badge/Package%20version-0.0.7-ff7675.svg)](commits/master)
[![License: apache-2.0](https://img.shields.io/badge/license-Apache%202-blue)](https://opensource.org/licenses/Apache-2.0)
<!-- badges: end -->


Geocoding is the process of taking a text-based description of a location, such as an address or the name of a place, and returning geographic coordinates, frequently latitude/longitude pair, to identify a location on the Earth's surface. The geokodeR package allows to convert the locations contained in a column of a dataframe into coordinates and others informations using these geocoding services: Nominatim, Here, Mapbox and Google.

######  **OpenStreetMap Nominatim service**
Nominatim is a tool to search OSM data by name and address (geocoding) and to generate synthetic addresses of OSM points (reverse geocoding): it can be found at [nominatim.openstreetmap.org](https://nominatim.openstreetmap.org/). For processing large amount of queries, please read the [Nominatim usage policy](https://operations.osmfoundation.org/policies/nominatim/), in short:
- send a maximum of 1 request per second;
- provide a valid HTTP Referer or User-Agent identifying the application;
- clearly display attribution as suitable for your medium;

######   **Here Geocoding API** 
The [HERE Geocoder API](https://developer.here.com/documentation/geocoding-search-api/dev_guide/topics/endpoint-geocode-brief.html) is a REST API service used to find the geo-coordinates of a known address, place, locality or administrative area, even if the query is incomplete or partly incorrect. It also returns a complete postal address string and address details.

######   **Mapbox Geocoding API** 
The [Mapbox Geocoding API](https://docs.mapbox.com/api/search/#geocoding) does two things:
- forward geocoding (converts location text into geographic coordinates);
- reverse geocoding (turns geographic coordinates into place names);

######   **Google Geocoding API** 
The [Google Geocoding API](https://developers.google.com/maps/documentation/geocoding/overview) is a service that provides geocoding and reverse geocoding of addresses.

#####  Cache logic
The package cache results to improve the performance and to reduce the load of requests in two ways:
- in a persist way, saving data into a redis database instance using the [redux package](https://github.com/richfitz/redux) (encapsulated in a R6 class);
- in a temporary way, saving data in a list type variable (**discouraged**, if the class is destroyed or the workplace cleaned up, the data will be lost);

#####  Output data
The package takes a dataframe as input and returns two types of datasets as the final output:
- *dataframe*, a table or a two-dimensional array-like structure in which each column contains values of one variable and each row contains one set of values from each column;
- *sf*, a collection of simple features that includes attributes and geometries in the form of a data frame. In other words, it is a data frame (or tibble) with rows of features, columns of attributes, and a special geometry column that contains the spatial aspects of the features;

The package adds these additional labels as new columns:
- *latitude*, geographic coordinate that specifies the north–south position of a point on the Earth's surface;
- *longitude*, geographic coordinate that specifies the east–west position of a point on the Earth's surface;
- *display_name*, complete name of the place;
- *accuracy*, number that indicates for each result how good it matches to the original query;
- *locality*, locality name;
- *province*, province name;
- *post_code*, series of letters or digits or both;
- *country*, country name;
- *country_code*, country code;
- *service*, name of the service that performed the geolocation.

If the chosen output is an sf object the latitude and longitude columns will not be visible: instead of the columns with the coordinates there will be the 'geometry' column that contains the data of the geographical point.

## Installation
```bash
devtools::install_gitlab("krpack/geokoder")
library(geokodeR)
```

### Init a redis instance
To cache the geocoding service results it's possible to initialize a Redis database instance (not mandatory). 
```r
redis <- geokodeR::redis_instance(host = '<ip address>', port = '<port>')
```
##### Prerequisite
It's possible install Redis both downloading the installer and have a stand-alone instance, or installing it by creating a Docker container using the Bitnami Redis Docker image.

##### Setup with Docker
To setup a Redis instance using Docker you can [follow those instructions](https://hub.docker.com/r/bitnami/redis/).

##### Setup from installer
To setup a Redis instance using the stand-alone version you can [follow those instructions](https://redis.io/download).

### Init a proxy instance
To send request from multiple server it's possible to initialize a proxy instance (not mandatory). 
```r
proxy <- geokodeR::princess_proxy_instance(host = '<ip address>', port = '<port>')
```
### Geocoding service initialization
The package allows you to initialize more than one geocoding service, some of which require an api key for data processing (Here, Mapbox and Google).

##### Nominatim instance
```r
# default initialization
# params: spleep_time=1, as_sf=FALSE, redis=FALSE, proxy=FALSE
nominatim <- geokodeR::nominatim_instance()
# with redis database instance
# params: spleep_time=1, as_sf=FALSE, redis=TRUE, proxy=FALSE
nominatim <- redis %>% 
                geokodeR::nominatim_instance()
# with redis database instance and proxy
# params: spleep_time=1, as_sf=FALSE, redis=TRUE, proxy=TRUE
nominatim <- geokodeR::nominatim_instance(redis, proxy)                
```
##### Here instance
```r
# default initialization
# params: spleep_time=1, as_sf=FALSE
here <- geokodeR::here_instance(key=<value>)
# with redis database instance
# params: spleep_time=1, as_sf=FALSE, redis=TRUE, proxy=FALSE
here <- redis %>% 
            geokodeR::here_instance(key=<value>)
# with redis database instance and proxy
# params: spleep_time=1, as_sf=FALSE, redis=TRUE, proxy=TRUE
here <- geokodeR::here_instance(key=<value>, redis, proxy)            
```
##### Mapbox instance
```r
# default initialization
# params: spleep_time=1, as_sf=FALSE
mapbox <- geokodeR::mapbox_instance(key=<value>)
# with redis database instance
# params: spleep_time=1, as_sf=FALSE, redis=TRUE, proxy=FALSE
mapbox <- redis %>% 
            geokodeR::mapbox_instance(key=<value>)
# with redis database instance and proxy
# params: spleep_time=1, as_sf=FALSE, redis=TRUE, proxy=TRUE
mapbox <- geokodeR::mapbox_instance(key=<value>, redis, proxy)            
```
##### Google instance
```r
# default initialization
# params: spleep_time=1, as_sf=FALSE
mapbox <- geokodeR::google_instance(key=<value>)
# with redis database instance
# params: spleep_time=1, as_sf=FALSE, redis=TRUE, proxy=FALSE
mapbox <- redis %>% 
            geokodeR::google_instance(key=<value>)
# with redis database instance and proxy
# params: spleep_time=1, as_sf=FALSE, redis=TRUE, proxy=TRUE
mapbox <- geokodeR::google_instance(key=<value>, redis, proxy)           
```
### Geocoding query customization
The package allows you to customize the parameters of a geocoding requests.

##### Nominatim query examples
```r
# dataframe
df <- data.frame(
        id = 1:3,
        city = c("Massa", "Pisa", "Milano")
      )
# nominatim instance       
nominatim <- geokodeR::nominatim_instance()      
# custom geocoding query: sleep time equal to two seconds
geocoding_query <- nominatim %>%
               geokodeR::sleep(2) 
# custom geocoding query: output as sf object              
geocoding_query <- nominatim %>%
               geokodeR::as_sf(T)
# custom geocoding query: overwriting of data stored in the cache             
geocoding_query <- nominatim %>%
               geokodeR::overwrite(T)               
# custom geocoding query: custom output columns      
geocoding_query <- nominatim %>%
               geokodeR::data(c('longitude', 'latitude') 
# custom geocoding query     
geocoding_query <- nominatim %>%
               geokodeR::overwrite(T) %>%
               geokodeR::sleep(2) %>%
               geokodeR::data(c('longitude', 'latitude')                
```
##### Here query examples
```r
# dataframe
df <- data.frame(
        id = 1:3,
        city = c("Massa", "Pisa", "Milano")
      )
# here instance
here <- geokodeR::here_instance(key=<value>)    
# custom geocoding query: sleep time equal to two seconds
geocoding_query <- here %>%
               geokodeR::sleep(2) 
# custom geocoding query: output as sf object              
geocoding_query <- here %>%
               geokodeR::as_sf(T)
# custom geocoding query: overwriting of data stored in the cache             
geocoding_query <- here %>%
               geokodeR::overwrite(T)               
# custom geocoding query: custom output columns      
geocoding_query <- here %>%
               geokodeR::data(c('longitude', 'latitude') 
# custom geocoding query     
geocoding_query <- here %>%
               geokodeR::overwrite(T) %>%
               geokodeR::sleep(2) %>%
               geokodeR::data(c('longitude', 'latitude')  
```
##### Mapbox query examples
```r
# dataframe
df <- data.frame(
        id = 1:3,
        city = c("Massa", "Pisa", "Milano")
      )
# mapbox instance      
mapbox <- geokodeR::mapbox_instance(key=<value>)    
# custom geocoding query: sleep time equal to two seconds
geocoding_query <- mapbox %>%
               geokodeR::sleep(2) 
# custom geocoding query: output as sf object              
geocoding_query <- mapbox %>%
               geokodeR::as_sf(T)
# custom geocoding query: overwriting of data stored in the cache             
geocoding_query <- mapbox %>%
               geokodeR::overwrite(T)               
# custom geocoding query: custom output columns      
geocoding_query <- mapbox %>%
               geokodeR::data(c('longitude', 'latitude') 
# custom geocoding query     
geocoding_query <- mapbox %>%
               geokodeR::overwrite(T) %>%
               geokodeR::sleep(2) %>%
               geokodeR::data(c('longitude', 'latitude')  
```
##### Google query examples
```r
# dataframe
df <- data.frame(
        id = 1:3,
        city = c("Massa", "Pisa", "Milano")
      )
# google instance         
google <- geokodeR::google_instance(key=<value>)   
# custom geocoding query: sleep time equal to two seconds
geocoding_query <- google %>%
               geokodeR::sleep(2) 
# custom geocoding query: output as sf object              
geocoding_query <- google %>%
               geokodeR::as_sf(T)
# custom geocoding query: overwriting of data stored in the cache             
geocoding_query <- google %>%
               geokodeR::overwrite(T)               
# custom geocoding query: custom output columns      
geocoding_query <- google %>%
               geokodeR::data(c('longitude', 'latitude') 
# custom geocoding query     
geocoding_query <- google %>%
               geokodeR::overwrite(T) %>%
               geokodeR::sleep(2) %>%
               geokodeR::data(c('longitude', 'latitude')  
```          
### Geocoding dataframe column
```r
# dataframe
df <- data.frame(
        id = 1:3,
        city = c("Massa", "Pisa", "Milano")
      )
# default geocoding query         
geocoding_instance 
# custom geocoding query
geocoding_query <- geocoding_instance %>%
               geokodeR::overwrite(T) %>%
               geokodeR::sleep(2) %>%
               geokodeR::data(c('longitude', 'latitude')  
# with custom params
geocoded_df <- geocoding_instance %>%
               geokodeR::geocode(df, 'city') 
# with default params
geocoded_df <- geocoding_query %>%
               geokodeR::geocode(df, 'city')  
               
```  
