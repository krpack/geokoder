#' Checks if a character vector is contained in another one
#'
#' @param vector_to_check the vector data to check
#' @param vector the vector data
#'
#' @return TRUE if the vecor is not contained in the other FALSE otherwise
.check_vector_data <- function (vector_to_check, vector) {

  if(!is.character(vector_to_check) || !is.character(vector)) {
    return(FALSE)
  }

  is_contained <- vector_to_check %in% vector
  return(FALSE %in% is_contained)

}

#' Checks that the values contained in a character vector are correctly encoded
#'
#' @param vector data vector to check
#'
#' @return FALSE if the column contain not ASCII character TRUE otherwise
.check_encoded_data_vector <- function (vector) {

  if(!is.character(vector)) {
    return(FALSE)
  }

  # remove NA values
  vector <- vector[!is.na(vector)]
  encoding_result <- tools::showNonASCII(vector)
  return(ifelse(length(encoding_result) > 0, FALSE, TRUE))

}

#' Checks if a server allows downloading data
#'
#' @param url http address to check
#'
#' @return TURE if the server allows downloading data, an error message otherwise
.check_scrapable_server <- function(url) {

  if(!stringr::str_detect(url, 'https?://*')) {
    return(FALSE)
  }

  url_parsed <- httr::parse_url(url)
  url_subdomain <- paste0(url_parsed$scheme, '://', url_parsed$hostname)
  robotstxt_information <- robotstxt::robotstxt(domain = url_subdomain)

  # Check that the server allows downloading data
  is_scrapable <- robotstxt_information$check(path = url_subdomain, bot = '*')
  if (!is_scrapable) {
    return(FALSE)
  }

  return(TRUE)

}


